package main.java.sidarta;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class OlaMundoBean {
	
	public String getHorario(){
		SimpleDateFormat dtf = new SimpleDateFormat("h:mm:ss");
		return "Atualizado em : " + dtf.format(new Date());
	}

}
