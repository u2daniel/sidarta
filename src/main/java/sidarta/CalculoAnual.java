package main.java.sidarta;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="calculoAnual",eager=true)
@RequestScoped
public class CalculoAnual implements Serializable{
	
	private int idade = 0;
	private int anoAtual = 0;
	private int resultado = 0;
	
	
	
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	
	public int getAnoAtual() {
		return anoAtual;
	}
	public void setAnoAtual(int anoAtual) {
		this.anoAtual = anoAtual;
	}
	
	
	public int getResultado() {
		
		resultado = anoAtual - idade;
		
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
	public String calcularNascimento(){
		
		resultado = anoAtual - idade;
		
		return "pagina1";		
		
	}

}
